declare name "kik";
declare nvoices "16";

noize	= component("noise.dsp");
sine	= component("sine.dsp");
beater  = component("beat.dsp");

vol 	= component("volume.dsp");
pan 	= component("panpot.dsp");
vu  	= component("vumeter.dsp").vmeter;
mute	= *(1 - checkbox("mute"));

voice(v)	= vgroup("Ch %v",  mute : hgroup("[2]", vol : vu) : pan);
stereo		= hgroup("stereo out", (vol, vol : vgroup("L", vu), vgroup("R", vu)));

process = sine,noize:hgroup("mixer", par(i, 2, voice(i)) :> stereo);
